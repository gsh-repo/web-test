# Gitlab CICD

## docker start gitlab-runner
```
docker run -d --name gitlab-runner --restart always \
-v /home/ubuntu/gitlab-cicd/gitlab-runner/config:/etc/gitlab-runner \
-v /var/run/docker.sock:/var/run/docker.sock \
gitlab/gitlab-runner:latest
```

## register gitlab runner to gitlab server

```
docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
--non-interactive \
--executor "docker" \
--docker-image alpine:latest \
--url "https://gitlab.com/" \
--registration-token "f8Me13TtPad7XRKybKys" \
--description "my first docker-runner" \
--tag-list "docekr-cicd" \
--run-untagged="true" \
--locked="false" \
--access-level="not_protected"
```
